/* 
 * Gnome Animal
 *
 * Copyright (C) 1999,2000 The Free Software Foundation
 *
 * Author: George Lebl <jirka@5z.com>
 */
#ifndef ANIMAL_H
#define ANIMAL_H

void show_windows (void);
void hide_windows (void);

void set_filename_prefix (const char *s);

void window_frame_iteration (void);
void window_move_iteration (void);

#endif
