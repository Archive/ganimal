/* 
 * Gnome Animal
 *
 * Copyright (C) 1999 The Free Software Foundation
 *
 * Author: George Lebl <jirka@5z.com>
 */

#include <config.h>
#include <gnome.h>
#include <stdlib.h>
#include <time.h>
#include <applet-widget.h>
#include <X11/Xlib.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libguile.h>
#include <guile/gh.h>

#include "animal.h"
#include "scheme-stuff.h"

#define DESTROY_TIMEOUT 60000

typedef struct _AnimalPix AnimalPix;
struct _AnimalPix {
	char *filename;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	int w,h;
	int destroy_timeout;
};

static GdkWindow *animal_window = NULL;
static GdkWindow *toy_window = NULL;
static GdkWindow *food_window = NULL;

static char *file_prefix = NULL;

static GHashTable *pixmap_cache_ht = NULL;

static GHashTable *pixmap_name_ht = NULL;

static const char *
name_intern (const char *name)
{
	const char *ret;

	if (name == NULL)
		return NULL;

	if (pixmap_name_ht == NULL)
		pixmap_name_ht = g_hash_table_new (g_str_hash, g_str_equal);

	ret = g_hash_table_lookup (pixmap_name_ht, (gpointer)name);
	if (ret == NULL) {
		char *foo = g_strdup (name);
		g_hash_table_insert (pixmap_name_ht, foo, foo);
		ret = foo;
	}

	return ret;
}

static void
make_garbage_pix (GdkPixmap **pixmap, GdkBitmap **mask, int width, int height)
{
	*pixmap = gdk_pixmap_new (NULL, width, height,
				  gdk_rgb_get_visual ()->depth);
	*mask = gdk_pixmap_new (NULL, width, height, 1);
}


static void
load_a_pix (AnimalPix *ap)
{
	GdkPixbuf *pixbuf;

	pixbuf = gdk_pixbuf_new_from_file (ap->filename);
	if (pixbuf == NULL) {
		make_garbage_pix (&ap->pixmap, &ap->mask, 100, 100);
		ap->w = 100;
		ap->h = 100;
	} else {
		gdk_pixbuf_render_pixmap_and_mask (pixbuf,
						   &ap->pixmap, &ap->mask,
						   127);
		ap->w = gdk_pixbuf_get_width (pixbuf);
		ap->h = gdk_pixbuf_get_height (pixbuf);
		gdk_pixbuf_unref (pixbuf);
	}
}

static int
pix_destroy_timeout (gpointer data)
{
	AnimalPix *ap = data;

	g_hash_table_remove (pixmap_cache_ht, ap->filename);
	g_free (ap->filename);
	ap->filename = NULL;
	gdk_pixmap_unref (ap->pixmap);
	ap->pixmap = NULL;
	gdk_bitmap_unref (ap->mask);
	ap->mask = NULL;
	gtk_timeout_remove (ap->destroy_timeout);
	ap->destroy_timeout = 0;
	g_free (ap);

	return FALSE;
}

static AnimalPix *
a_get_pix (const char *filename)
{
	char *file = g_concat_dir_and_file (file_prefix, filename);
	AnimalPix *ret;

	if (pixmap_cache_ht == NULL)
		pixmap_cache_ht = g_hash_table_new (g_str_hash, g_str_equal);

	ret = g_hash_table_lookup (pixmap_cache_ht, file);
	if (ret != NULL) {
		g_free (file);
		gtk_timeout_remove (ret->destroy_timeout);
		ret->destroy_timeout = gtk_timeout_add (DESTROY_TIMEOUT,
							pix_destroy_timeout,
							ret);
		return ret;
	}

	ret = g_new0 (AnimalPix, 1);
	/* Takes over ownership of the "file" string */
	ret->filename = file;
	ret->destroy_timeout = gtk_timeout_add (DESTROY_TIMEOUT,
						pix_destroy_timeout,
						ret);
	load_a_pix (ret);
	g_hash_table_insert (pixmap_cache_ht, ret->filename, ret);
	return ret;
}

static GdkFilterReturn
animal_filter (GdkXEvent *xev, GdkEvent *ev, gpointer data)
{
	XEvent *xevent = (XEvent *)xev;

	if (xevent->type == ButtonPress) {
		do_animal_clicked (xevent->xbutton.x, xevent->xbutton.y);

		return GDK_FILTER_REMOVE;
	}
	return GDK_FILTER_CONTINUE;
}

static GdkWindow *
make_pix_window (char * (*pix_func)(void), GdkFilterFunc ffunc)
{
	GdkWindow *win;
	GdkWindowAttr attributes;
	gint attributes_mask;
	AnimalPix * pix;
	char * fname;
	int x,y;

	fname = (*pix_func) ();
	pix = a_get_pix (fname);
	g_free (fname);

	attributes.window_type = GDK_WINDOW_TEMP;
	attributes.width = pix->w;
	attributes.height = pix->h;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gdk_rgb_get_visual ();
	attributes.colormap = gdk_rgb_get_cmap ();
	attributes.event_mask = GDK_BUTTON_PRESS_MASK;
	/*(GDK_EXPOSURE_MASK |
				 GDK_KEY_PRESS_MASK |
				 GDK_ENTER_NOTIFY_MASK |
				 GDK_LEAVE_NOTIFY_MASK |
				 GDK_FOCUS_CHANGE_MASK |
				 GDK_STRUCTURE_MASK |
				 GDK_BUTTON_PRESS_MASK);*/

	attributes_mask = GDK_WA_VISUAL | GDK_WA_COLORMAP;

	win = gdk_window_new (NULL, &attributes, attributes_mask);

	gdk_window_shape_combine_mask (win, pix->mask, 0, 0);
	gdk_window_set_back_pixmap (win, pix->pixmap, FALSE);
	gdk_window_clear (win);

	if (ffunc != NULL)
		gdk_window_add_filter (win, ffunc, NULL);

	return win;
}

void
set_filename_prefix (const char *s)
{
	g_free (file_prefix);
	file_prefix = g_strdup (s);
}

void
show_windows (void)
{
	int x, y;

	if (animal_window == NULL)
		animal_window = make_pix_window (animal_pix, animal_filter);
	if (toy_window == NULL)
		toy_window = make_pix_window (toy_pix, NULL);
	if (food_window == NULL)
		food_window = make_pix_window (food_pix, NULL);

	animal_xy (&x, &y);
	gdk_window_move (animal_window, x, y);
	toy_xy (&x, &y);
	gdk_window_move (toy_window, x, y);
	food_xy (&x, &y);
	gdk_window_move (food_window, x, y);

	if (animal_shown ())
		gdk_window_show (animal_window);
	if (toy_shown ())
		gdk_window_show (toy_window);
	if (food_shown ())
		gdk_window_show (food_window);
}
void
hide_windows (void)
{
	if (animal_window != NULL)
		gdk_window_hide (animal_window);
	if (toy_window != NULL)
		gdk_window_hide (toy_window);
	if (food_window != NULL)
		gdk_window_hide (food_window);
}

static void
frame_iteration_for_window (GdkWindow *win,
			    char * (*pix_func) (void))
{
	AnimalPix *pix;
	char *fname;
	const char *old_fname;
	const char *new_fname;
	static GQuark file_quark = 0;
	
	if (file_quark == 0)
		file_quark = g_quark_from_static_string ("file");

	fname = (*pix_func) ();

	new_fname = name_intern (fname);
	old_fname = g_dataset_id_get_data (win, file_quark);

	g_free (fname);

	if (old_fname == new_fname)
		return;

	/* set data to the interned pointer */
	g_dataset_id_set_data (win, file_quark, (gpointer)new_fname);

	pix = a_get_pix (new_fname);

	gdk_window_resize (win, pix->w, pix->h);

	gdk_window_shape_combine_mask (win, pix->mask, 0, 0);
	gdk_window_set_back_pixmap (win, pix->pixmap, FALSE);
	gdk_window_clear (win);
}

void
window_frame_iteration (void)
{
	if (animal_window != NULL)
		frame_iteration_for_window (animal_window, animal_pix);
	if (toy_window != NULL)
		frame_iteration_for_window (toy_window, toy_pix);
	if (food_window != NULL)
		frame_iteration_for_window (food_window, food_pix);
}

static void
move_iteration_for_window (GdkWindow *win,
			   void (*getcoord) (int *, int *))
{
	int x, y;

	(*getcoord) (&x, &y);
	gdk_window_move (win, x, y);
}

void
window_move_iteration (void)
{
	if (animal_window != NULL)
		move_iteration_for_window (animal_window, animal_xy);
	if (toy_window != NULL)
		move_iteration_for_window (toy_window, toy_xy);
	if (food_window != NULL)
		move_iteration_for_window (food_window, food_xy);
}
