/* 
 * Gnome Animal
 *
 * Copyright (C) 1999 The Free Software Foundation
 *
 * Author: George Lebl <jirka@5z.com>
 */

#include <config.h>
#include <gnome.h>
#include <stdlib.h>
#include <time.h>
#include <applet-widget.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libguile.h>
#include <guile/gh.h>

#include "scheme-stuff.h"
#include "animal.h"

static SCM show_animal;
static SCM show_toy;
static SCM show_food;

static SCM current_animal_pix;
static SCM current_toy_pix;
static SCM current_food_pix;

static SCM animal_clicked;
static SCM toy_throw;
static SCM animal_frame_iteration;
static SCM animal_move_iteration;

static SCM animal_getx;
static SCM animal_gety;
static SCM toy_getx;
static SCM toy_gety;
static SCM food_getx;
static SCM food_gety;

gboolean
animal_shown (void)
{
	return gh_scm2bool (gh_call0 (show_animal));
}
gboolean
toy_shown (void)
{
	return gh_scm2bool (gh_call0 (show_toy));
}
gboolean
food_shown (void)
{
	return gh_scm2bool (gh_call0 (show_food));
}

#define RETURN_GLIB(s) { char *ret = g_strdup (s); free (s); return (ret); }

char *
animal_pix (void)
{
	char *s = gh_scm2newstr (gh_call0 (current_animal_pix), NULL);
	RETURN_GLIB (s);
}
char *
toy_pix (void)
{
	char *s = gh_scm2newstr (gh_call0 (current_toy_pix), NULL);
	RETURN_GLIB (s);
}
char *
food_pix (void)
{
	char *s = gh_scm2newstr (gh_call0 (current_food_pix), NULL);
	RETURN_GLIB (s);
}

void
animal_xy (int *x, int *y)
{
	if (x != NULL)
		*x = gh_scm2long (gh_call0 (animal_getx));
	if (y != NULL)
		*y = gh_scm2long (gh_call0 (animal_gety));
}
void
toy_xy (int *x, int *y)
{
	if (x != NULL)
		*x = gh_scm2long (gh_call0 (toy_getx));
	if (y != NULL)
		*y = gh_scm2long (gh_call0 (toy_gety));
}
void
food_xy (int *x, int *y)
{
	if (x != NULL)
		*x = gh_scm2long (gh_call0 (food_getx));
	if (y != NULL)
		*y = gh_scm2long (gh_call0 (food_gety));
}

void
do_animal_clicked (int x, int y)
{
	gh_call2 (animal_clicked, gh_long2scm (x), gh_long2scm (y));
}


static SCM
scm_human_grab_toy (void)
{
	/* FIXME: grab the toy here */
	return SCM_EOL;
}

void
init_scheme_stuff (void)
{
	char *s;

	s = g_strdup_printf ("(define screen-width %d)\n"
			     "(define screen-height %d)\n",
			     gdk_screen_width (), gdk_screen_height ());
	gh_eval_str (s);
	g_free (s);

	gh_new_procedure0_0 ("human-grab-toy", scm_human_grab_toy);
}

/* cut of the .scm from the file and make that a prefix for the images */
static void
set_prefix (const char *animal)
{
	char *s,*p;
	int len = strlen (animal) + 2;
	s = g_new (char, len);
	memcpy (s, animal, len);
	p = strrchr (s, '.');
	if (p != NULL)
		*p = '\0';
	/* we know we have the space for one more character */
	strcat (s, "/");
	set_filename_prefix (s);
	g_free (s);
}

void
load_animal_file (const char *animal)
{
	/* init so that we get a sane enviroment to work in */
	init_scheme_stuff ();

	if (g_file_exists (animal)) {
		int i;

		const struct {
			SCM *scm;
			char *name;
		} functions[] = {
			{ &show_animal, "show-animal?" },
			{ &show_toy, "show-toy?" },
			{ &show_food, "show-food?" },

			{ &current_animal_pix, "current-animal-pix" },
			{ &current_toy_pix, "current-toy-pix" },
			{ &current_food_pix, "current-food-pix" },

			{ &animal_clicked, "animal-clicked" },
			{ &toy_throw, "toy-throw" },
			{ &animal_frame_iteration, "animal-frame-iteration" },
			{ &animal_move_iteration, "animal-move-iteration" },

			{ &animal_getx, "animal-getx" },
			{ &animal_gety, "animal-gety" },
			{ &toy_getx, "toy-getx" },
			{ &toy_gety, "toy-gety" },
			{ &food_getx, "food-getx" },
			{ &food_gety, "food-gety" },

			{ NULL, NULL }
		};

		/* gh_ is not const correct, but this works fine */
		gh_eval_file ((char *)animal);

		set_prefix (animal);

		for (i = 0; functions[i].scm != NULL; i++) {
			*(functions[i].scm) = gh_eval_str (functions[i].name);
		}
	} else {
		g_warning("FIXME: look for file in default location");
	}
}

void
do_frame_iteration (int x, int y)
{
	gh_call2 (animal_frame_iteration, gh_long2scm (x), gh_long2scm (y));
}

void
do_move_iteration (int x, int y)
{
	gh_call2 (animal_move_iteration, gh_long2scm (x), gh_long2scm (y));
}
