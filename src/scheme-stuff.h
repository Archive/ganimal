/* 
 * Gnome Animal
 *
 * Copyright (C) 1999,2000 The Free Software Foundation
 *
 * Author: George Lebl <jirka@5z.com>
 */
#ifndef SCHEME_STUFF_H
#define SCHEME_STUFF_H

void init_scheme_stuff (void);
void load_animal_file (const char *animal);

gboolean animal_shown (void);
gboolean toy_shown (void);
gboolean food_shown (void);

char * animal_pix (void);
char * toy_pix (void);
char * food_pix (void);

void animal_xy (int *x, int *y);
void toy_xy (int *x, int *y);
void food_xy (int *x, int *y);

void do_animal_clicked (int x, int y);

void do_frame_iteration (int x, int y);
void do_move_iteration (int x, int y);

#endif
