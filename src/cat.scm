;GnomeAnimal: Kitty
; The above line is for parsed by ganimal
;
; Cat animal description cat.scm
;
; Copyright (C) 1999 The Free Software Foundation
;
; Author: George Lebl <jirka@5z.com>

(define cur-animal-frame 0)
(define cur-toy-frame 0)
(define cur-food-frame 0)
(define animal-change-to '())

; Some constants
(define toy-drag 0.05) ; drag per iteration (note: newacc = acc * (1 - drag))

(define (goto-animal-state state)
  (begin 
    (set! animal-state state)
    (set! cur-animal-frame 0)
    (set! animal-change-to '())))

(define (goto-animal-transient-state state next)
  (begin 
    (set! animal-state state)
    (set! cur-animal-frame 0)
    (set! animal-change-to next)))

(define (goto-toy-state state)
  (begin 
    (set! toy-state state)
    (set! cur-toy-frame 0)))

(define (goto-food-state state)
  (begin 
    (set! food-state state)
    (set! cur-food-frame 0)))

; get the current frame from a frameset
(define (get-animal-frame frameset)
  (cond ((null? animal-change-to)
	 (let ((current (modulo cur-animal-frame (length frameset))))
	   (list-ref frameset current)))
	(else 
	  (cond ((>= cur-animal-frame (length frameset))
		 (begin
		   (goto-animal-state animal-change-to)
		   (list-ref frameset cur-animal-frame)))
		(else
		  (list-ref frameset cur-animal-frame))))))
(define (get-toy-frame frameset)
  (let ((current (modulo cur-toy-frame (length frameset))))
    (list-ref frameset current)))
(define (get-food-frame frameset)
  (let ((current (modulo cur-food-frame (length frameset))))
    (list-ref frameset current)))

; increase the current frame number
(define (inc-animal-frame)
  (set! cur-animal-frame (+ cur-animal-frame 1)))
(define (inc-toy-frame)
  (set! cur-toy-frame (+ cur-toy-frame 1)))
(define (inc-food-frame)
  (set! cur-food-frame (+ cur-food-frame 1)))

(define (within-square? sq-coord1 sq-coord2 coord)
  (cond ((and (<= (car sq-coord1) (car coord))
	      (<= (cdr sq-coord1) (cdr coord))
	      (>= (car sq-coord2) (car coord))
	      (>= (cdr sq-coord2) (cdr coord))) #t)
	(else #f)))

(define run-n-pix '("cat-run-n-1.png"
		    "cat-run-n-2.png"))
(define run-nw-pix '("cat-run-nw-1.png"
		     "cat-run-nw-2.png"))
(define run-w-pix '("cat-run-w-1.png"
		    "cat-run-w-2.png"))
(define run-sw-pix '("cat-run-sw-1.png"
		     "cat-run-sw-2.png"))
(define run-s-pix '("cat-run-s-1.png"
		    "cat-run-s-2.png"))
(define run-se-pix '("cat-run-se-1.png"
		     "cat-run-se-2.png"))
(define run-e-pix '("cat-run-e-1.png"
		    "cat-run-e-2.png"))
(define run-ne-pix '("cat-run-ne-1.png"
		     "cat-run-ne-2.png"))

(define sleep-pix '("cat-sleep-1.png"
		    "cat-sleep-2.png"))

(define play-pix '("cat-play-1.png"
		   "cat-play-2.png"))
; the location of the toy on the animal as it's playing
; so that we can take it away
(define play-toy-location 
  (list (list (cons 10 10) (cons 20 20))
	(list (cons 20 20) (cons 30 30))))

(define eat-pix '("cat-eat-1.png"
		  "cat-eat-2.png"
		  "cat-eat-3.png"))

(define wakeup-pix '("cat-wakeup-1.png"
		     "cat-wakeup-2.png"))

(define sit-pix '("cat-sit-1.png"
		  "cat-sit-2.png"))

(define hungry-pix '("cat-hungry-1.png"
		     "cat-hungry-2.png"))

(define food-full-pix '("cat-food-full.png"))

(define food-empty-pix '("cat-food-empty.png"))

(define toy-rest-pix '("toy-rest.png"))

(define toy-move-pix '("toy-move-1.png"
		       "toy-move-2.png"))

(define run-speed 3)

; animal state can be 'sleep 'run-{n,nw,w,sw,s,se,e,ne} 'play 'eat 'hungry or 'sit
(define animal-state 'sleep)
(define animal-coord (cons
		       (- screen-width 200)
		       (- screen-height 200)))

; toy state can be 'alone 'animal or 'human
(define toy-state 'alone)
(define toy-coord (cons 50 50))
(define toy-acc (cons 20 20))

; food state can be 'shown or 'hidden
(define food-state 'hidden)
; amount can be 'full or 'empty
(define food-amount 'full)
(define food-coord (cons 300 300))

; Accessors
(define (animal-getx) (car animal-coord))
(define (animal-gety) (cdr animal-coord))
(define (toy-getx) (car toy-coord))
(define (toy-gety) (cdr toy-coord))
(define (food-getx) (car food-coord))
(define (food-gety) (cdr food-coord))

; show animal at all times
(define (show-animal?) #t)

; show toy when not with animal
(define (show-toy?)
  (not (eq? toy-state 'animal)))

(define (show-food?)
  (eq? food-state 'shown))

(define (current-animal-pix)
  (get-animal-frame 
    (cond ((eq? animal-state 'play) play-pix)
	  ((eq? animal-state 'eat) eat-pix)
	  ((eq? animal-state 'sit) sit-pix)
	  ((eq? animal-state 'wakeup) wakeup-pix)
	  ((eq? animal-state 'sleep) sleep-pix)
	  ((eq? animal-state 'hungry) hungry-pix)
	  ((eq? animal-state 'run-n) run-n-pix)
	  ((eq? animal-state 'run-nw) run-nw-pix)
	  ((eq? animal-state 'run-w) run-w-pix)
	  ((eq? animal-state 'run-sw) run-sw-pix)
	  ((eq? animal-state 'run-s) run-s-pix)
	  ((eq? animal-state 'run-se) run-se-pix)
	  ((eq? animal-state 'run-e) run-e-pix)
	  ((eq? animal-state 'run-ne) run-ne-pix)
	  (else '("unknown.png")))))

(define (current-toy-pix)
  (cond ((eq? toy-state 'human) (get-toy-frame toy-rest-pix))
	((eq? toy-state 'alone) (get-toy-frame toy-rest-pix))
	(else "")))

(define (current-food-pix)
  (cond ((eq? food-state 'hidden) "")
	((eq? food-amount 'full) (get-food-frame food-full-pix))
	((eq? food-amount 'empty) (get-food-frame food-empty-pix))
	(else "")))

; called when the human clicks on an animal to take away the toy
(define (animal-clicked x y)
  (cond ((and (eq? animal-state 'play)
	      (within-square?
		(car (play-pix 'get-data))
		(car (cdr (play-pix 'get-data)))
		(cons x y)))
	 (begin
	   (goto-toy-state 'human)
	   (goto-animal-state 'sit)
	   (human-grab-toy)))
	((eq? animal-state 'sleep)
	 (begin
	   (goto-animal-transient-state 'wakeup 'sit)))))

; called by the code when the human throws the toy at x y with acceleration
; of xacc and yacc
(define (toy-throw x y xacc yacc)
  ; FIXME: actually have the thing move
  (set-car! toy-coord x)
  (set-cdr! toy-coord y)
  (set-car! toy-acc xacc)
  (set-cdr! toy-acc yacc)
  (goto-toy-state 'alone))

(define (negate-car a)
  (set-car! a (- (car a))))
(define (negate-cdr a)
  (set-cdr! a (- (cdr a))))

(define (out-of-range a lo hi)
  (or (< a lo) (> a hi)))

; Move the toy according to it's acceleration
(define (move-toy)
  (let ((x (car toy-coord))
	(y (cdr toy-coord))
	(xacc (car toy-acc))
	(yacc (cdr toy-acc)))
   (if (or (not (= xacc 0))
	   (not (= yacc 0)))
    (begin 
      (set-car! toy-coord (truncate (+ x xacc)))
      (set-cdr! toy-coord (truncate (+ y yacc)))
      (set-car! toy-acc (* xacc (- 1 toy-drag)))
      (set-cdr! toy-acc (* yacc (- 1 toy-drag)))

      ; Need ... new ... definitions
      (let ((x (car toy-coord))
	    (y (cdr toy-coord))
	    (xacc (car toy-acc))
	    (yacc (cdr toy-acc)))
	(if (out-of-range x 0 screen-width)
	  (begin
	    (negate-car toy-coord)
	    (negate-car toy-acc)))
	(if (out-of-range y 0 screen-height)
	  (begin
	    (negate-cdr toy-coord)
	    (negate-cdr toy-acc))))))))


; called all the time to get frame updates on the animal
(define (animal-frame-iteration x y)
  (inc-animal-frame)
  (inc-toy-frame)
  (inc-food-frame))

; called all the time to get move updates on the animal
(define (animal-move-iteration x y)
  ; FIXME: make the animal move
  (move-toy))
