/* 
 * Gnome Animal
 *
 * Copyright (C) 1999 The Free Software Foundation
 *
 * Author: George Lebl <jirka@5z.com>
 */

#include <config.h>
#include <gnome.h>
#include <stdlib.h>
#include <time.h>
#include <applet-widget.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libguile.h>
#include <guile/gh.h>

#include "scheme-stuff.h"
#include "animal.h"

static AppletWidget *applet = NULL;
static int size = PIXEL_SIZE_STANDARD;
static PanelOrientType orient = ORIENT_UP;

static GtkWidget *darea;

static guchar *back_rgb = NULL;
static int back_w = 0;
static int back_h = 0;
static int back_rs = 0;

static gboolean is_animal_shown = FALSE;

static void
about (AppletWidget *applet, gpointer data)
{
	static const char *authors[] = {
		"George Lebl <jirka@5z.com>",
		NULL
	};
	GtkWidget *about_box;

	about_box = gnome_about_new (_("The Gnome Animal"),
				     VERSION,
				     _("Copyright (C) 1999,2000 The Free Software Foundation"),
				     authors,
				     _("A Gnome pet animal"),
				     NULL);

	gtk_widget_show(about_box);
}

static void
darea_draw(void)
{
	int i;
	guchar *p;
	guchar *rgb_buffer;

	if(!GTK_WIDGET_REALIZED(darea) ||
	   !GTK_WIDGET_DRAWABLE(darea))
		return;

	if(!back_rgb)
		applet_widget_get_rgb_bg(applet,&back_rgb,
					 &back_w,&back_h,&back_rs);
	if(!back_rgb)
		return;

	rgb_buffer = g_new(guchar,back_h*back_rs);
	memcpy(rgb_buffer,back_rgb,back_h*back_rs*sizeof(guchar));
	for(i=0,p=rgb_buffer;i<back_h*back_rs;i+=4,p+=4)
		*p >>= 1;

	gdk_draw_rgb_image(darea->window,darea->style->white_gc,
			   0,0, back_w, back_h,
			   GDK_RGB_DITHER_NORMAL,
			   rgb_buffer, back_rs);

	g_free(rgb_buffer);
}

static int iteration_timeout = 0;

static int
iteration_timeout_func (gpointer data)
{
	static int iteration = 5;
	int x, y;
	gdk_window_get_pointer (NULL, &x, &y, NULL);

	do_move_iteration (x, y);
	window_move_iteration ();

	if (iteration == 5) {
		do_frame_iteration (x, y);

		window_frame_iteration ();
		iteration = 0;
	}

	iteration ++;

	return TRUE;
}

static int
applet_button_event (GtkWidget *w, GdkEventButton *e)
{
	if (e->button == 1) {
		is_animal_shown = ! is_animal_shown;
		if (is_animal_shown) {
			show_windows ();
			if ( ! iteration_timeout) {
				iteration_timeout =
					gtk_timeout_add (100,
							 iteration_timeout_func,
							 NULL);
			}
		} else {
			hide_windows ();
			if (iteration_timeout != 0) {
				gtk_timeout_remove (iteration_timeout);
				iteration_timeout = 0;
			}
		}
	}
	return FALSE;
}

static void
remake_pet_home (void)
{
	gtk_drawing_area_size (GTK_DRAWING_AREA (darea), size, size);
	g_free (back_rgb);
	back_rgb = NULL;

	/*FIXME: remake the pet home here */
	gtk_widget_queue_draw (darea);
}

static void
applet_change_pixel_size(GtkWidget *w, int sz, gpointer data)
{
	/* if we haven't really changed anything */
	if(size == sz)
		return;
	orient = applet_widget_get_panel_orient(applet);
	size = sz;
	remake_pet_home();
}

static void
applet_change_orient(GtkWidget *w, PanelOrientType o, gpointer data)
{
	/* if we haven't really changed anything */
	if(orient == o)
		return;
	orient = o;
	size = applet_widget_get_panel_pixel_size(applet);
	remake_pet_home();
}

static void
applet_do_draw(GtkWidget *w, gpointer data)
{
	g_free(back_rgb);
	back_rgb = NULL;

	gtk_widget_queue_draw(darea);
}

static void
main_prog(int argc, char *argv[])
{
	init_scheme_stuff();
	load_animal_file("cat.scm");
	applet_widget_gtk_main ();
}

int
main (int argc, char **argv)
{
	GtkWidget *applet_widget;

	/*bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);*/
	
	srand(time(NULL));

	applet_widget_init ("ganimal_applet", VERSION, argc,
			    argv, NULL, 0, NULL);

	applet_widget = applet_widget_new ("ganimal_applet");
	if (!applet_widget)
		g_error (_("Can't create gnome animal applet!"));

	applet = APPLET_WIDGET(applet_widget);
	
	size = applet_widget_get_panel_pixel_size(APPLET_WIDGET(applet));
	orient = applet_widget_get_panel_orient(APPLET_WIDGET(applet));

	gtk_widget_push_visual (gdk_rgb_get_visual ());
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());
	darea = gtk_drawing_area_new();
	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();

	gtk_widget_set_events(GTK_WIDGET(applet),
			      gtk_widget_get_events(GTK_WIDGET(applet)) |
			      GDK_BUTTON_PRESS_MASK);

	gtk_drawing_area_size(GTK_DRAWING_AREA(darea),size,size);

	remake_pet_home();

	gtk_signal_connect(GTK_OBJECT(applet),"change_pixel_size",
			   GTK_SIGNAL_FUNC(applet_change_pixel_size),
			   NULL);
	gtk_signal_connect(GTK_OBJECT(applet),"change_orient",
			   GTK_SIGNAL_FUNC(applet_change_orient),
			   NULL);
	gtk_signal_connect(GTK_OBJECT(applet),"do_draw",
			   GTK_SIGNAL_FUNC(applet_do_draw),
			   NULL);
	applet_widget_send_draw(applet,TRUE);

	applet_widget_add(APPLET_WIDGET(applet), darea);
	gtk_signal_connect(GTK_OBJECT(darea),"draw",
			   GTK_SIGNAL_FUNC(darea_draw),
			   NULL);
	gtk_signal_connect(GTK_OBJECT(darea),"expose_event",
			   GTK_SIGNAL_FUNC(darea_draw),
			   NULL);
	gtk_signal_connect(GTK_OBJECT(applet),"button_press_event",
			   GTK_SIGNAL_FUNC(applet_button_event),
			   NULL);
	gtk_widget_show(darea);
	gtk_widget_show(applet_widget);

	applet_widget_register_stock_callback (APPLET_WIDGET (applet),
					       "about",
					       GNOME_STOCK_MENU_ABOUT,
					       _("About..."),
					       about,
					       NULL);

	gh_enter(argc, argv, main_prog);

	return 0;
}
